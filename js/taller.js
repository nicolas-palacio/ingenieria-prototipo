let url=new URL(window.location.href);
let string=url.search;

let stringSplit=string.split("?id=");
let urlID=stringSplit[1].split("?")[0];
console.log("El id del taller es "+urlID);

const postWrap=document.getElementById("post-wrap");
let fragment=document.createDocumentFragment();
let coordinates=[];

let popUpText=``;
const showWorkshop=()=>{
    publicaciones.forEach(elem=>{
        if(elem.id==urlID){
            postWrap.innerHTML=`  
                                <div class="post-wrap__img"><img src="${elem.foto}" alt=""></div>
                                <div class="post-wrap__description">
                                    <h2>${elem.nombre}</h2>
                                    <hr>
                                    <p class="post-category">Categoria:${elem.categoria}</p>
                                    <p class="post-description">Actividades:${elem.actividades}</p>
                                    <p class="post-address">Direccion: ${elem.calle +" "+elem.numero+", "+elem.localidad}</p>   
                                    <p class="post-phone">Telefono: ${elem.telefonoTaller}</p>
                                    <p class="post-time">Horario: ${elem.horario}</p>

                                    <div id="map"></div>
                                </div>  `
                       
            coordinates=[elem.coordenadaY,elem.coordenadaX];
            popUpText=`<h1>${elem.nombre}</h2><p>Telefono: ${elem.telefonoTaller}</p> <p>Horario: ${elem.horario}</p>`; 
        }      
    });
}

showWorkshop();
createMap("map",coordinates,popUpText);
