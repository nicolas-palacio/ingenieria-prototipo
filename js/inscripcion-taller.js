const form=document.querySelector("form");
const formWrap=document.getElementById("form-wrap");
let isFormValid=false;

const nameInput= document.getElementById("name");
const descriptionInput= document.getElementById("description-input");
const activitiesInput=document.getElementById("activities");
const categoryBoxForm=document.getElementById("category");
const timeInput=document.getElementById("form-time");
const phoneInput=document.getElementById("form-phone");
const workshopPhoto=document.getElementById("workshop-photo");

const productsWrap=document.getElementById("wrap");
const closeList=document.getElementById("close-list");
const addressList=document.getElementById("list");
const addresInput=document.getElementById("adress");
const addressNumber=document.getElementById("adress_number");
const cityName=document.getElementById("city");
let isAddressNormalize=false;
let mustShowMap=false;
let fragment=document.createDocumentFragment();
var ungsLocation = [-34.5221554, -58.7000067];
let coordinateX=0;
let coordinateY=0;

const successfulMessage=document.querySelector(".successful-message");
const postWrap=document.getElementById("post-wrap");
let file="";

//Carga las categorias
categories.forEach(category=>{
    const option = document.createElement("option");
    option.innerHTML=` <option value="empty" selected>${category}</option>`
    categoryBoxForm.appendChild(option);
});

const btnNormalize= document.getElementById("show-address-list");


btnNormalize.addEventListener("click",()=>{
    if(!addresInput.value && !addressNumber.value && !cityName.value){
        alert("Debe rellenar los valores en la seccion Direccion")
    } 

    if(addresInput.value && addressNumber.value && cityName.value){
        addressList.classList.remove("hidden");
        formWrap.classList.add("margin-left");
        document.getElementById("map").classList.remove("hidden")
        ajax();
        mustShowMap=true;
        
    } else{
        addressList.classList.remove("hidden");
        formWrap.classList.add("margin-left");    
        ajax();  
    }
    
});



let ajax=()=>{
    productsWrap.innerHTML=``;
    const http= new XMLHttpRequest();
    const url= API_ADDRESS_URL+`${addresInput.value}`+` ${addressNumber.value},`+` ${cityName.value}`;

    http.onreadystatechange= ()=>{
        if(http.readyState== 4 && http.status== 200){
            let json= JSON.parse(http.responseText);
            console.log(json.direccionesNormalizadas);

            let results=json.direccionesNormalizadas;
            results.forEach(elem =>{
                const itemDiv = document.createElement("div");
                itemDiv.classList.add("product");
                itemDiv.innerHTML=`  
                    <div class="product__img">                           
                        </div>
                        <div class="product__description">
                            <h3 class="name">Direccion</h3>
                            <p class="price">Calle: ${elem.nombre_calle}</p>
                            <p class="price">Localidad: ${elem.nombre_localidad}</p>
                            <p class="price">Partido: ${elem.nombre_partido}</p>
                        </div>
                    </div>`

                    if((elem.tipo=="calle_altura" || elem.tipo=="calle_calle") && results.length==1){
                        coordinateX=elem.coordenadas.x;
                        coordinateY=elem.coordenadas.y;
                        mustShowMap=true;
                    }
              
                    fragment.appendChild(itemDiv);

            });
            if(mustShowMap){
                
                createMap("map",[coordinateY,coordinateX],"");
                addressNormalized();
            }
            productsWrap.appendChild(fragment);
            
        }
    };
   
    http.open("GET",url);
    http.send();
    
}

const addressNormalized=()=>{
    document.getElementById("normalize-success").classList.remove("hidden");
    isAddressNormalize=true;
    addresInput.classList.add("border-succes");
    addressNumber.classList.add("border-succes");
    cityName.classList.add("border-succes");

};

closeList.addEventListener("click",()=>{
    closeEvent();
 });

 const closeEvent=()=>{
    addressList.classList.add("hidden");
    formWrap.classList.remove("margin-left");
    document.getElementById("map").classList.add("hidden");
 }
 workshopPhoto.addEventListener('change', function(e) {
    file = workshopPhoto.files[0];
    console.log(file);
});

form.addEventListener("submit",(e)=>{
    e.preventDefault();
    validateInputs();
    if(isFormValid && isAddressNormalize){
        form.remove();
        successfulMessage.classList.remove("hidden");
        showWorkshopPost();
        
    }
});


const validateInputs=()=>{
    isFormValid=true;

    if(!nameInput.value){
        invalidateElm(nameInput);
        isFormValid=false;
        
    }

    if(!descriptionInput.value){
        invalidateElm(descriptionInput);
        isFormValid=false;
        
    }

    if(!activitiesInput.value){
        invalidateElm(activitiesInput);
        isFormValid=false;
        
    }

    if(categoryBoxForm.value=='empty'){
        invalidateElm(categoryBoxForm);
        isFormValid=false;
    }

    if(!timeInput.value){
        invalidateElm(timeInput);
        isFormValid=false;       
    }

    if(!phoneInput.value){
        invalidateElm(phoneInput);
        isFormValid=false;    
    }

    if(workshopPhoto.files[0]==undefined){
        invalidateElm(workshopPhoto);
        isFormValid=false;
         
    } 
    if(!isAddressNormalize){
        alert("Es necesario normalizar la direccion del taller.")
        isFormValid=false;
    }
}

const invalidateElm=(elem)=>{
    elem.classList.add('invalid');
    elem.nextElementSibling.classList.remove("hidden");
};

const showWorkshopPost=()=>{
    closeEvent();
    document.getElementById("title-h2").classList.add("hidden");
    document.getElementById("title-p").classList.add("hidden"); 
    postWrap.classList.add("post-wrap");
    postWrap.innerHTML=`  
                                <div class="post-wrap__img" id="post-img"><img src="" alt=""></div>
                                <div class="post-wrap__description">
                                    <h2>${nameInput.value}</h2>
                                    <hr>
                                    <p class="post-category">Categoria: ${categoryBoxForm.value}</p>
                                    <p class="post-activities">Actividades: ${activitiesInput.value}</p>
                                    <p class="post-description">Descripcion: ${descriptionInput.value}</p>
                                    <p class="post-address">Direccion: ${addresInput.value +" "+addressNumber.value+", "+cityName.value}</p>   
                                    <p class="post-phone">Telefono: ${phoneInput.value}</p>
                                    <p class="post-phone">Horario: ${timeInput.value}</p>
                                    <div id="map-post"></div>
                                </div>  `
    insertImg();
    createMap("map-post",[coordinateY,coordinateX],`<h1>${nameInput.value}</h2><p>Telefono: ${phoneInput.value}</p> <p>Horario: ${timeInput.value}</p>`);
}

const insertImg=()=>{
    let reader = new FileReader();
    const divImg=document.getElementById("post-img")
    reader.onload = function(e) {
        divImg.innerHTML = "";

         let img = new Image();
         img.src = reader.result;

         divImg.appendChild(img);
     }

     reader.readAsDataURL(file); 

}