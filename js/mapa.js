const createMap=(nameId,location,popUpText)=>{

    var iconoBase = L.Icon.extend({
        options: {
            iconSize:     [38, 55],
            iconAnchor:   [22, 94],
            popupAnchor:  [-3, -76]
        }
    });
    
    var iconoTaller = new iconoBase({iconUrl: '../imgs/localizacionIcon.png'});
          
        // Creación del componente mapa de Leaflet.
    var map = L.map(nameId).setView(location, 15);
    
        // Agregamos los Layers de OpenStreetMap.
    var baseLayer = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);
    
    var estiloPopup = {'maxWidth': '200'}
    L.marker(location, {icon: iconoTaller}).bindPopup(popUpText,estiloPopup)
    .addTo(map);
}

const mapWithoutLocations=(nameId)=>{

    var iconoBase = L.Icon.extend({
        options: {
            iconSize:     [38, 55],
            iconAnchor:   [22, 94],
            popupAnchor:  [-3, -76]
        }
    });
    
   
          
        // Creación del componente mapa de Leaflet.
    var map = L.map(nameId).setView([-34.558484,-58.88484], 15);
    
        // Agregamos los Layers de OpenStreetMap.
    var baseLayer = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);
    
    var estiloPopup = {'maxWidth': '200'}
    
    return map;
}

const buildMap=(mapa,location,popUpText)=>{
    let map = mapa
    var iconoTaller = new iconoBase({iconUrl: '../imgs/localizacionIcon.png'});
    L.marker(location, {icon: iconoTaller}).bindPopup(popUpText,estiloPopup)
    .addTo(map);
}