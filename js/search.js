let url=new URL(window.location.href);
console.log(url);
console.log(url.search);
let string=url.search;

let stringSplit=string.split("?name=");
let urlName=stringSplit[1].split("?")[0];
console.log("La busqueda es "+urlName);

let stringSplitCat=string.split("?category=");
let UrlCategory=stringSplitCat[1].split("?")[0]
console.log("La categoria es: "+UrlCategory);


let stringSplitType=string.split("?type=");
let UrlType=stringSplitType[1].split("?")[0]
console.log("El tipo es: "+UrlType);

const resultTitle=document.getElementById("search-result-title");

resultTitle.textContent=`Resultados de la busqueda "${urlName}"`

const searchWrap=document.getElementById("search-wrap");
let fragment=document.createDocumentFragment();
let elementAlreadyInserted=false;

let listSearchItems=()=>{
    publicaciones.forEach(elem =>{
        if(elem.nombre.toLowerCase().includes(urlName.toLowerCase()) && urlName!=""){
            insertDOMElement(elem);
            elementAlreadyInserted=true;
        }

        if(elem.categoria==UrlCategory && !elementAlreadyInserted){
            insertDOMElement(elem);
            elementAlreadyInserted=true;
        }

        if(elem.tipo==UrlType && !elementAlreadyInserted){
            insertDOMElement(elem);
            elementAlreadyInserted=true;
        }
        
        if(urlName=="" && UrlType=="empty" && UrlCategory=="empty"){
            insertDOMElement(elem);
            elementAlreadyInserted=true;
        }
        elementAlreadyInserted=false;
    });
   
    
};


let insertDOMElement=(elem)=>{
    if(elem.tipo=="artesania"){
        const itemDiv = document.createElement("div");
                                itemDiv.classList.add("products-search");
                                itemDiv.innerHTML=`  
                                    <div class="product-search__img">
                                    <img src="${elem.foto}"/>
                                    </div>
                                <div class="product-search__description">
                                    <h3 class="name">${elem.nombre}</h3>
                                    <p class="type">Artesania</p>
                                    <p class="price">${elem.precio}</p>
                                    <p class="category">${elem.categoria}</p>                                  
                                </div>   `
        addListenerCraft(itemDiv,elem);
        fragment.appendChild(itemDiv);
    }else{
        const itemDiv = document.createElement("div");
                                itemDiv.classList.add("products-search");
                                itemDiv.innerHTML=`  
                                    <div class="product-search__img">
                                    <img src="${elem.foto}"/>
                                    </div>
                                <div class="product-search__description">
                                    <h3 class="name">${elem.nombre}</h3>
                                    <p class="type">Taller</p>
                                    <p class="price">${elem.calle +" "+ elem.numero +", " + elem.localidad}</p>
                                    <p class="description">Telefono: ${elem.telefonoTaller}</p>
                                    <p class="category">Horario: ${elem.horario}</p>                                 
                                </div>   `
                addEventListenerWork(itemDiv,elem);
                fragment.appendChild(itemDiv);
    }
};

const addListenerCraft=(div,elem)=>{
    div.addEventListener("click",()=>{
        window.location=(`artesania.html?id=${elem.id}`)
    })
}

const addEventListenerWork=(div,elem)=>{
    div.addEventListener("click",()=>{
        window.location=(`taller.html?id=${elem.id}`)
    })
}

listSearchItems();
searchWrap.appendChild(fragment);


