const searchInput=document.getElementById("search-input");
const subMenu=document.getElementById("sub-menu");
const categoryBox=document.getElementById("category-search");
const typeBox=document.getElementById("type-search");
const subMenuCloseIcon=document.getElementById("close-sub-menu");
const searchBtn=document.getElementById("search-btn");

//Carga las categorias
categories.forEach(category=>{
    const option = document.createElement("option");
    option.innerHTML=` <option value="${category}" selected>${category}</option>`
    categoryBox.appendChild(option);
});

searchInput.addEventListener("focus",()=>{
    subMenu.classList.remove("hidden");
})

searchInput.addEventListener("keypress",(e)=>{
   if(e.key=="Enter"){
        search();
   }
})

subMenuCloseIcon.addEventListener("click",()=>{
    subMenu.classList.add("hidden");
})

searchBtn.addEventListener("click",()=>{
    search();
})




const search=()=>{
    console.log("type: "+`${typeBox.value}`)
    if(`${typeBox.value}` == "taller"){
        window.location=(`searchTaller.html?name=${searchInput.value}?category=${categoryBox.value}?type=${typeBox.value}`)
    }else{
        window.location=(`search.html?name=${searchInput.value}?category=${categoryBox.value}?type=${typeBox.value}`)
    }
}