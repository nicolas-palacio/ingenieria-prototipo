let url=new URL(window.location.href);
let string=url.search;

let stringSplit=string.split("?id=");
let urlID=stringSplit[1].split("?")[0];
console.log("El id es "+urlID);

const postchWrap=document.getElementById("post-wrap");
let fragment=document.createDocumentFragment();

const showCraft=()=>{
    publicaciones.forEach(elem=>{
        if(elem.id==urlID){
            postchWrap.innerHTML=`  
                                <div class="post-wrap__img"><img src="${elem.foto}" alt=""></div>
                                <div class="post-wrap__description">
                                    <h2>${elem.nombre}</h2>
                                    <hr>
                                    <p class="post-category">${elem.categoria}</p>
                                    <p class="post-price">${elem.precio}</p>
                                    <p class="post-description">${elem.descripcion}</p>
                                    <p class="post-commercialization">${elem.formaDeComerciar}</p>
                                    <p class="post-name">Nombre del artesano: ${elem.nombreArtesano}</p>
                                    <p class="post-phone">Telefono: ${elem.telefonoArtesano}</p>
                                    <p class="post-email">Correo: ${elem.correoArtesano}</p>
                                </div>  `
                       
        }      
    });
}

showCraft();