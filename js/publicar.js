const form=document.querySelector("form");
const formWrap=document.getElementById("form-wrap");
let isFormValid=false;

/*Inputs del formulario*/ 
const nameInput= document.getElementById("name");
const categoryBoxForm=document.getElementById("category");
const descriptionInput= document.getElementById("description-input");
const typeBoxForm=document.getElementById("post-type");
const priceDiv=document.getElementById("price-div");
const priceInput=document.getElementById("price-input");
const extraDescriptionDiv= document.getElementById("extra-description-div");
const extraDescriptionInput= document.getElementById("extra-description-input");
const crafterName=document.getElementById("form-craft-name");
const crafterPhone=document.getElementById("form-craft-phone");
const crafterMail=document.getElementById("form-email");
const craftPhoto=document.getElementById("craft-photo-input");

let file="";
let fragment=document.createDocumentFragment();

const productsWrap=document.getElementById("wrap");
const pricesList=document.getElementById("list");
const btnShowList=document.getElementById("show-list");
const closeList=document.getElementById("close-list");

const postWrap=document.getElementById("post-wrap");
const successfulMessage=document.querySelector(".successful-message");

//Carga las categorias
categories.forEach(category=>{
    const option = document.createElement("option");
    option.innerHTML=` <option value="empty" selected>${category}</option>`
    categoryBoxForm.appendChild(option);
});
form.addEventListener('submit',(e)=>{
    e.preventDefault();
    validateInputs();
    if(isFormValid){
        form.remove();
        successfulMessage.classList.remove("hidden");
        showCraftPost();
        
    }
});

const validateInputs= () =>{
    resetElement(nameInput);
    resetElement(descriptionInput);
    resetElement(categoryBoxForm);
    resetElement(typeBoxForm);
    resetElement(priceInput);
    isFormValid=true;
    
    if(!nameInput.value){
        invalidateElm(nameInput);
        isFormValid=false;
        console.log("nameInput "+ isFormValid);  
        
    }

    if(!descriptionInput.value){
        invalidateElm(descriptionInput);
        console.log("descriptionInput "+ isFormValid);  
        isFormValid=false; 
        
       
    }

    if(categoryBoxForm.value=='empty'){
        invalidateElm(categoryBoxForm);
        isFormValid=false;
        
    }

    if(typeBoxForm.value=='empty'){
        invalidateElm(typeBoxForm);
        isFormValid=false;
        
    }

    if(!priceInput.value && !isHidden(priceDiv)){
        invalidateElm(priceInput);
        console.log("priceInput "+ isFormValid);  
        isFormValid=false;
        
    }

    if(!extraDescriptionInput.value && !isHidden(extraDescriptionDiv)){
        invalidateElm(extraDescriptionInput);
        console.log("extraDescriptionInput "+ isFormValid);  
        isFormValid=false;
        
    }

    if(!crafterName.value){
        invalidateElm(crafterName);
        console.log("crafterName "+ isFormValid);  
        isFormValid=false;
    }

    if(!crafterPhone.value){
        invalidateElm(crafterPhone);
        console.log("crafterPhone "+ isFormValid);  
        isFormValid=false;
    }

    if(!crafterMail.value){
        invalidateElm(crafterMail);
        console.log("crafterMail "+ isFormValid);  
        isFormValid=false;
         
    } 
    
    if(craftPhoto.files[0]==undefined){
        invalidateElm(craftPhoto);
        console.log("craftPhoto "+ isFormValid);  
        isFormValid=false;
         
    }   
};

craftPhoto.addEventListener('change', function(e) {
    file = craftPhoto.files[0];
    console.log(file);
   //file.type.match(imageType)
       
   
});



const removeEmptyOption=()=>{
    const emptyOption=document.getElementById("empty-option");
    if(emptyOption!=null){
        typeBoxForm.remove(0);
    }
}

const resetElement= (elem)=>{
    elem.classList.remove('invalid');
    elem.nextElementSibling.classList.add("hidden");
};

const invalidateElm=(elem)=>{
    elem.classList.add('invalid');
    elem.nextElementSibling.classList.remove("hidden");
};

const isHidden=(elem)=>{
    return elem.classList.contains("hidden");
 };

 typeBoxForm.addEventListener("change",()=>{
    let type=typeBoxForm.options[typeBoxForm.selectedIndex].value;
   
    if(type=="venta"){
        priceDiv.classList.remove("hidden");
        extraDescriptionDiv.classList.add("hidden");
        removeEmptyOption();
    }else{
        extraDescriptionDiv.classList.remove("hidden");
        priceDiv.classList.add("hidden");
        removeEmptyOption();
    }
   
});


btnShowList.addEventListener("click",()=>{
     if(!nameInput.value){
        alert("Debe ingresar un nombre para ver la lista.")
     }else{
        pricesList.classList.remove("hidden");
        formWrap.classList.add("margin-left")
        ajax();
     }
    
 });

 closeList.addEventListener("click",()=>{
    pricesList.classList.add("hidden");
    formWrap.classList.remove("margin-left")
    productsWrap.innerHTML=``;
 });


let ajax=()=>{
    productsWrap.innerHTML=``;
    const http= new XMLHttpRequest();
    const url=API_PRICES_URL+`${nameInput.value}`;

    http.onreadystatechange= ()=>{
        if(http.readyState== 4 && http.status== 200){
            let json= JSON.parse(http.responseText);
            console.log(json.results);

            let results=json.results;
            results.forEach(elem =>{
                const itemDiv = document.createElement("div");
                itemDiv.classList.add("product");
                itemDiv.innerHTML=`  
                    <div class="product__img">
                            <img src="${elem.thumbnail}"/>
                        </div>
                        <div class="product__description">
                            <h3 class="name">${elem.title}</h3>
                            <p class="price">Precio $${elem.price}</p>                          
                        </div>
                    </div>`
               
                    fragment.appendChild(itemDiv);
            });
           
            productsWrap.appendChild(fragment);
        }
    };
   
    http.open("GET",url);
    http.send();
}

const showCraftPost=()=>{
    document.getElementById("title-h2").classList.add("hidden");
    document.getElementById("title-p").classList.add("hidden");
    postWrap.classList.remove("hidden");
    postWrap.innerHTML=`  
                                <div class="post-wrap__img" id="post-img"><img src="" alt=""></div>
                                <div class="post-wrap__description">
                                    <h2>${nameInput.value}</h2>
                                    <hr>
                                    <p class="post-category">${categoryBoxForm.value}</p>
                                    <p class="post-price">${priceInput.value}</p>
                                    <p class="post-description">${descriptionInput.value}</p>
                                    <p class="post-commercialization">${extraDescriptionInput.value}</p>
                                    <p class="post-name">Nombre del artesano: ${crafterName.value}</p>
                                    <p class="post-phone">Telefono: ${crafterPhone.value}</p>
                                    <p class="post-email">Correo: ${crafterMail.value}</p>
                                </div>  `
    insertImg();
}

const insertImg=()=>{
    let reader = new FileReader();
    const divImg=document.getElementById("post-img")
    reader.onload = function(e) {
        divImg.innerHTML = "";

         let img = new Image();
         img.src = reader.result;

         divImg.appendChild(img);
     }

     reader.readAsDataURL(file); 

}
//ajax();